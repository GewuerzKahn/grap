

#include <GLUT/glut.h>
#include <stdlib.h>


#include "math.h"


#include "OwnSquare.h"
#include "OwnPolygon.h"
#include "OwnCube.h"
#include "OwnTetrahedron.h"
#include "Camera.h"
#include "OwnCone.h"
#include "LightSource.h"
#include "enums.h"
#include "OwnCheckerboard.h"
#include "loadBMP.h"
#include "imageLoader.h"
#include "OwnFont.h"

enum MENU_TYPE {
    M_TASKS,
    M_SHAPE_LINE,
    M_SHAPE_FILL,
    M_SHAPE_STIPPLE,
    M_SD_DIAGONALS,
    M_LINEWIDTH_1,
    M_LINEWIDTH_2,
    M_WIREFRAME,
    M_BEAMCUBE,
    M_DEFAULT_POSITION,
    M_MOVE_LIGHT,
    M_SHOWMENU
};

float width, heigth;

Camera *camera;

OwnSquare *square;
OwnPolygon *polygon;
OwnCube *cube1, *cube2;
LightSource *lightSource;
OwnTetrahedron *tetra;
OwnCone *cone1, *cone2;
OwnCheckerboard *checker1;

OwnFont *fontB;

bool toogleTasks = false;
bool fpsMode = true;
bool mouseLeftDown = false;
bool mouseRightDown = false;
bool keyPressed[256];
bool warped = false;
bool wireframeMode = false;

const GLfloat cameraSpeed = 0.05f;
const GLfloat cameraRotationSpeed = 0.00003;


GLint gFramesPerSecond = 0;
GLint lastMousePositionX = 0;
GLint lastMousePositionY = 0;


MENU_TYPE show = M_SHOWMENU;

Image image;
Image image2;



void menu(int);
void drawAxis(GLfloat metric);
void specialKeyboard(int key, int kx, int ky);
void fpsKeyboard();
void drawFrame();
GLuint loadTexture(Image* image);

void init(void) {

    glShadeModel(GL_SMOOTH);

    //camera = new Camera();

    square = new OwnSquare();
    polygon = new OwnPolygon();
    cube1 = new OwnCube();
    cube2 = new OwnCube();
    tetra = new OwnTetrahedron();
    cone1 = new OwnCone();
    cone2 = new OwnCone();
    checker1 = new OwnCheckerboard();
    fontB = new OwnFont();

    lightSource = new LightSource(2, 2, -3);


    cube1->setPosition(-2, 0, -5);
    cube2->setPosition(2, 0, -5);
    tetra->setPosition(0, 0, -5);
    cone1->setPosition(-2, 0, -2);
    cone1->setMaterial(EnumMaterial::BRASS);
    cone2->setPosition(2, 0, -2);
    cone2->setMaterial(EnumMaterial::RED_PLASTIK);
    checker1->setPosition(0.0, 0.0, 0.0);


    ImageLoader *il = new ImageLoader();


    bmpLoad("opengl.bmp", &image);
    bmpLoad("ballon.bmp", &image2);

    GLuint textureId1 = loadTexture(&image);
    GLuint textureId2 = loadTexture(&image2);

    cube1->setTextId(textureId1, il->getTexture(""));

    //glShadeModel(GL_FLAT);
    //glMatrixMode(GL_MODELVIEW);
}

void FPS(void) {
    static GLint Frames = 0; // frames averaged over 1000mS
    static GLuint Clock; // [milliSeconds]
    static GLuint PreviousClock = 0; // [milliSeconds]
    static GLuint NextClock = 0; // [milliSeconds]

    ++Frames;
    Clock = glutGet(GLUT_ELAPSED_TIME); //has limited resolution, so average over 1000mS
    if (Clock < NextClock) return;

    gFramesPerSecond = Frames / 1; // store the averaged number of frames per second

    PreviousClock = Clock;
    NextClock = Clock + 1000; // 1000mS=1S in the future
    Frames = 0;
}

void timer(int value) {
    const int desiredFPS = 120;
    glutTimerFunc(1000 / desiredFPS, timer, ++value);
    FPS(); //only call once per frame loop to measure FPS 

    fpsKeyboard();

    glutPostRedisplay();
}

void display(void) {
    if (wireframeMode) {
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

        glDisable(GL_NORMALIZE);
        glDisable(GL_LIGHTING);
        glDisable(GL_LIGHT0);
        glDisable(GL_DEPTH_TEST);


    } else {
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

        //        glEnable(GL_NORMALIZE);
        //        glEnable(GL_LIGHTING);
        //        glEnable(GL_LIGHT0);
        glEnable(GL_DEPTH_TEST);

    }

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    //lightSource->draw();

    //   camera->refresh();

    //        square->draw();
    cube1->draw();
    //        cube2->draw();
    //
    //        tetra->draw();
    //
    //        cone1->draw();
    //
    //        glPushMatrix();
    //        glPopMatrix();
    //        cone2->draw();

    //    checker1->draw();

    fontB->draw();
    // drawFrame();


    //drawAxis(1.0);

    glutSwapBuffers();

}

void reshape(int w, int h) {

    width = w;
    heigth = h;

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glViewport(0, 0, (GLsizei) w, (GLsizei) h);
    gluPerspective(65.0, GLfloat(w) / GLfloat(h), 0.1, 100.0);
    glMatrixMode(GL_MODELVIEW);



}

void fontReshape(int w, int h) {
    width = w;
    heigth = h;

    /* Set the transformations */
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(-1.0, 1.0, -1.0, 1.0, -1.0, 1.0);
    glMatrixMode(GL_MODELVIEW);
    glViewport(0, 0, w, h);
}

void myReshape(GLsizei w, GLsizei h) {
    glViewport(0, 0, w, h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    if (w <= h) {
        gluOrtho2D(-2.0, 2.0, -2.0 * (GLfloat) h / (GLfloat) w, 2.0 * (GLfloat) h / (GLfloat) w);
    } else {
        gluOrtho2D(-2.0 * (GLfloat) w / (GLfloat) h, 2.0 * (GLfloat) w / (GLfloat) h, -2.0, 2.0);
    }
    glMatrixMode(GL_MODELVIEW);
}

void mouse(int button, int state, int x, int y) {
    switch (button) {
        case GLUT_LEFT_BUTTON:
            if (state == GLUT_DOWN) {
                mouseLeftDown = true;
            } else {
                mouseLeftDown = false;
            }
            break;
        case GLUT_RIGHT_BUTTON:
            if (state == GLUT_DOWN) {

                mouseRightDown = true;
            } else {
                mouseRightDown = false;
            }
            break;
        default:
            break;
    }

    lastMousePositionX = x;
    lastMousePositionY = y;
}

void keyboard(unsigned char key, int x, int y) {
    switch (key) {

        case 49:
            checker1->setSize(64);
            break;

        case 50:
            checker1->setSize(128);
            break;

        case 51:
            checker1->setSize(256);
            break;

        case 52:
            break;

        case 53:
            square->setDiagonalsVisbility(false);
            break;

        case 54:
            square->setLineWidth(1.0f);
            break;

        case 55:
            camera->setPosition(-7.40, 4.35, 5.85);
            camera->setPitch(0);
            camera->setYaw(0);
            break;

        case 56:
            camera->setPosition(-3, 3, -8);
            camera->setPitch(-0.58);
            camera->setYaw(0.95);
            break;

        case 27:
            exit(0);
            break;

        case 32:
            if (wireframeMode) {
                wireframeMode = false;
            } else {
                wireframeMode = true;
            }
            break;




    }
    keyPressed[key] = true;

}

void keyboardUp(unsigned char key, int x, int y) {
    keyPressed[key] = false;
}

void specialKeyboard(unsigned char key, int x, int y) {


}

void fpsKeyboard() {

    if (fpsMode) {
        if (keyPressed['w'] || keyPressed['W']) {
            camera->move(cameraSpeed);
        } else if (keyPressed['s'] || keyPressed['S']) {
            camera->move(-cameraSpeed);
        }
        if (keyPressed['a'] || keyPressed['A']) {
            camera->strafe(cameraSpeed);
        } else if (keyPressed['d'] || keyPressed['D']) {
            camera->strafe(-cameraSpeed);
        }
        if (keyPressed['r'] || keyPressed['R']) {
            camera->fly(cameraSpeed);
        } else if (keyPressed['f'] || keyPressed['F']) {
            camera->fly(-cameraSpeed);
        }

        if (keyPressed['m'] || keyPressed['M']) {
            GLfloat cameraPitchAngle;
            camera->getPitch(cameraPitchAngle);
            camera->setPitch(cameraPitchAngle - 0.03f);
        } else if (keyPressed['k'] || keyPressed['K']) {
            GLfloat cameraPitchAngle;
            camera->getPitch(cameraPitchAngle);
            camera->setPitch(cameraPitchAngle + 0.03f);
        }
        if (keyPressed['j'] || keyPressed['J']) {
            GLfloat cameraYawAngle;
            camera->getYaw(cameraYawAngle);
            camera->setYaw(cameraYawAngle - 0.03f);
        } else if (keyPressed['l'] || keyPressed['L']) {
            GLfloat cameraYawAngle;
            camera->getYaw(cameraYawAngle);
            camera->setYaw(cameraYawAngle + 0.03f);
        }


    }
}

void mouseMotion(int x, int y) {
    //    glutSetCursor(GLUT_CURSOR_NONE);

    if (fpsMode) {

        GLint dx = lastMousePositionX - x;
        GLint dy = lastMousePositionY - y;

        lastMousePositionX = x;
        lastMousePositionY = y;

        if (mouseLeftDown) {
            GLfloat angleX, angleY, angleZ;
            cube1->getAngle(angleX, angleY, angleZ);

            angleX -= dx * 0.6f;
            angleY -= dy * 0.6f;

            cube1->setAngle(angleX, angleY, 0.0);
        }

        //        glutWarpPointer(width / 2, heigth / 2);
        //        camera->rotateYaw(cameraRotationSpeed * dx);
        //        camera->rotatePitch(-cameraRotationSpeed * dy);
    }
}

void createMenu() {
    glutCreateMenu(menu);

    glutAddMenuEntry("Switch between tasks", M_TASKS);

    glutAddMenuEntry("Shape line", M_SHAPE_LINE);
    glutAddMenuEntry("Shape fill", M_SHAPE_FILL);
    glutAddMenuEntry("Shape stipple", M_SHAPE_STIPPLE);
    glutAddMenuEntry("Show/Hide diagonals", M_SD_DIAGONALS);
    glutAddMenuEntry("Toggle wirefrane", M_WIREFRAME);
    glutAddMenuEntry("Beam cube", M_BEAMCUBE);
    glutAddMenuEntry("Set default position", M_DEFAULT_POSITION);
    glutAddMenuEntry("De-/Active moving lightsource", M_MOVE_LIGHT);
    glutAddMenuEntry("Line width 1", M_LINEWIDTH_1);
    glutAddMenuEntry("Line width 2", M_LINEWIDTH_2);

    glutAttachMenu(GLUT_RIGHT_BUTTON);


}

int main(int argc, char** argv) {


    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    glutInitWindowSize(800, 600);
    glutInitWindowPosition(100, 100);
    glutCreateWindow("Banane");
    init();
    createMenu();
    glutDisplayFunc(display);

    //3D
    glutReshapeFunc(fontReshape);
//    glClearColor(1.0, 1.0, 1.0, 1.0);
    glColor3f(0.0, 0.0, 0.0);
    glPointSize(5.0);
    glEnable(GL_MAP1_VERTEX_3);
    //2D
    // glutReshapeFunc(myReshape);

    glutMouseFunc(mouse);
    glutMotionFunc(mouseMotion);
    glutPassiveMotionFunc(mouseMotion);
    glutKeyboardFunc(keyboard);
    glutKeyboardUpFunc(keyboardUp);
    //    glutSpecialFunc(specialKeyboard);

    //glutTimerFunc(0, timer, 0);

    glutMainLoop();
    return 0;
}

void menu(int item) {
    switch (item) {
        case M_SHOWMENU:
        {
            show = (MENU_TYPE) item;
        }
            break;

        case M_TASKS:
            if (toogleTasks) {
                toogleTasks = false;
            } else {
                toogleTasks = true;
            }
            break;


        case M_SHAPE_LINE:
            square->changeSquareState(1);
            break;

        case M_SHAPE_FILL:
            square->changeSquareState(2);
            break;

        case M_SHAPE_STIPPLE:
            square->changeSquareState(3);
            break;

        case M_SD_DIAGONALS:
            if (square->isDiagonalsVisible()) {
                square->setDiagonalsVisbility(false);
            } else {
                square->setDiagonalsVisbility(true);
            }
            break;

        case M_LINEWIDTH_1:
            square->setLineWidth(1.0f);
            break;

        case M_LINEWIDTH_2:
            square->setLineWidth(5.0f);
            break;

        case M_WIREFRAME:
            if (wireframeMode) {
                wireframeMode = false;
            } else {
                wireframeMode = true;
            }
            break;

        case M_BEAMCUBE:
            cube2->setScale(2, 1, 1);
            cube2->setPosition(1, 0, -5);
            break;

        case M_DEFAULT_POSITION:
            camera->setPosition(-3, 3, -8);
            camera->setPitch(-0.58);
            camera->setYaw(0.95);
            break;

        case M_MOVE_LIGHT:
            lightSource->toggleMovingLight();
            break;

        default:
        {
            /* Nothing */        }
            break;
    }

    return;
}

void drawAxis(GLfloat metric) {

    glPushMatrix();

    glEnable(GL_LINE_WIDTH);
    glLineWidth(5.0); // 1.0 is default
    glBegin(GL_LINES);
    glColor3f(0, 0, 1);
    glVertex3f(0.0, 0.0, 0.0);
    glVertex3f(metric, 0.0, 0.0);
    glVertex3f(0.0, 0.0, 0.0);
    glVertex3f(0.0, metric, 0.0);
    glVertex3f(0.0, 0.0, 0.0);
    glVertex3f(0.0, 0.0, metric);
    glEnd();
    glEnable(GL_LINE_STIPPLE);
    glLineStipple(3, 0xcccc);
    glBegin(GL_LINES);
    glColor3f(0, 0, 1);
    glVertex3f(0.0, 0.0, 0.0);
    glVertex3f(-metric, 0.0, 0.0);
    glVertex3f(0.0, 0.0, 0.0);
    glVertex3f(0.0, -metric, 0.0);
    glVertex3f(0.0, 0.0, 0.0);
    glVertex3f(0.0, 0.0, -metric);
    glEnd();
    glDisable(GL_LINE_STIPPLE);

    glPopMatrix();
}

void drawFrame() {
    glRasterPos3f(0, 0, 0);
    //glPixelZoom(2.0, -2.0);
    glDrawPixels(image.sizeX, image.sizeY, GL_RGB, GL_UNSIGNED_BYTE, image.data);
}

GLuint loadTexture(Image* image) {
    GLuint textureId;
    glGenTextures(1, &textureId);
    glBindTexture(GL_TEXTURE_2D, textureId);
    glTexImage2D(GL_TEXTURE_2D, //Always GL_TEXTURE_2D
            0, //0 for now
            GL_RGB, //Format OpenGL uses for image
            image->sizeX, image->sizeY, //Width and height
            0, //The border of the image
            GL_RGB, //GL_RGB, because pixels are stored in RGB format
            GL_UNSIGNED_BYTE, //GL_UNSIGNED_BYTE, because pixels are stored
            //as unsigned numbers
            image->data);
    return textureId;
}
/* 
 * File:   OwnTetrahedron.cpp
 * Author: student
 * 
 * Created on September 17, 2014, 2:29 PM
 */

#include "OwnTetrahedron.h"

OwnTetrahedron::OwnTetrahedron() {
}

OwnTetrahedron::OwnTetrahedron(const OwnTetrahedron& orig) {
}

OwnTetrahedron::~OwnTetrahedron() {
}

void OwnTetrahedron::draw() {
    glPushMatrix();

    //glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

    angle = 45;
    glTranslatef(x, y, z);
    //glRotatef(angle, 1.0, 0.0, 0.0);
    //glRotatef(30, 0.0, 0.0, 1.0);


    glEnableClientState(GL_VERTEX_ARRAY);
    //  glEnableClientState(GL_COLOR_ARRAY);

    // glColorPointer(3, GL_FLOAT, 0, colors);
    glVertexPointer(3, GL_FLOAT, 0, vertices);
    // glDrawArrays(GL_POLYGON, 0, 8);
    glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_SHORT, indices);

    glDisableClientState(GL_VERTEX_ARRAY);
    //   glDisableClientState(GL_COLOR_ARRAY);
    glPopMatrix();
}




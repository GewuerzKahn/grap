/* 
 * File:   OwnPolygon.h
 * Author: student
 *
 * Created on September 11, 2014, 9:35 PM
 */

#ifndef OWNPOLYGON_H
#define	OWNPOLYGON_H

#include <GLUT/glut.h>

class OwnPolygon {
public:
    OwnPolygon();
    OwnPolygon(const OwnPolygon& orig);
    virtual ~OwnPolygon();

    void draw();
private:
     GLfloat vertices[18] = {
        -0.5f, 0.5f, 0.0f,
        0.5f, 0.5f, 0.0f,
        1.0f, 0.0f, 0.0f,
        0.5f, -0.5f, 0.0f,
        -0.5f, -0.5f, 0.0f,
        -1.0f, 0.0f, 0.0f
    };
     
     GLfloat colors[18] = {
        1.0, 0.0, 0.0,
        0.0, 1.0, 0.0,
        0.0, 0.0, 1.0,
        1.0, 0.0, 0.0,
        0.0, 1.0, 0.0,
        0.0, 0.0, 1.0
    };
};

#endif	/* OWNPOLYGON_H */


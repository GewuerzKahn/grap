/* 
 * File:   loadBMP.h
 * Author: dos
 *
 * Created on July 6, 2009, 7:29 PM
 * Based on function found in Ulrich Jacobs final assignment, no source given
 */

#ifndef _LOADBMP_H
#define	_LOADBMP_H

#ifdef	__cplusplus
extern "C" {
#endif

#include <GLUT/glut.h>
  
struct Image
{
	unsigned long sizeX;
	unsigned long sizeY;
	GLubyte *data;
};

typedef struct Image Image;

/* 
 * pre  : filename is name of a valid .bmp file
 * post : image contains a array of color values RGB, ready for use in a texture
 *        return value = 1 when succesfull otherwise 0
 */
int bmpLoad(char* filename, Image *image);

#ifdef	__cplusplus
}
#endif

#endif	/* _LOADBMP_H */


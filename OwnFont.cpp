/* 
 * File:   OwnFont.cpp
 * Author: student
 * 
 * Created on October 5, 2014, 1:44 PM
 */

#include "OwnFont.h"

OwnFont::OwnFont() {
}

OwnFont::OwnFont(const OwnFont& orig) {
}

OwnFont::~OwnFont() {
}

void OwnFont::draw() {
    glBegin(GL_POINTS);
    for (int i = 0; i < ncpts; i++)
        glVertex3fv(cpts[i]);
    glEnd();

    drawCurves();

}

void OwnFont::drawCurves() {
    int i;
    GLfloat gain;

    // determin gain
    for (i = 0; i < ncpts; i++) {
        cpts[i][0] = (-16.0 + cpts[i][0]) / 20.0;
        cpts[i][1] = (-16.0 + cpts[i][1]) / 20.0;
        cpts[i][2] = cpts[i][2];
        //printf("\n %d %f %f",i,cpts[i][0],cpts[i][1]);
    }

    /* Draw the curves */
    glColor3f(1.0, 0.0, 0.0);
    for (i = 0; i < OUTER_CPTS - 3; i += 3) {
        /* Draw the outer curve using OpenGL evaluators */
        glMap1f(GL_MAP1_VERTEX_3, 0.0, 1.0, 3, 4, cpts[i]);
        glMapGrid1f(30, 0.0, 1.0);
        glEvalMesh1(GL_LINE, 0, 30);
    }
    for (i = 0; i < DOT_CPTS - 3; i += 3) {
        /* Draw the inner curve using OpenGL evaluators */
        glMap1f(GL_MAP1_VERTEX_3, 0.0, 1.0, 3, 4, cpts[OUTER_CPTS + i]);
        glMapGrid1f(30, 0.0, 1.0);
        glEvalMesh1(GL_LINE, 0, 30);
    }
}


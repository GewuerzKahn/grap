/* 
 * File:   OwnCube.cpp
 * Author: student
 * 
 * Created on September 17, 2014, 1:13 PM
 */

#include "OwnCube.h"

OwnCube::OwnCube() {

    angle = 0;
}

OwnCube::OwnCube(const OwnCube& orig) {
}

OwnCube::~OwnCube() {
}

void OwnCube::draw() {
    glPushMatrix();

    glScalef(scaleX, scaleY, scaleZ);

    glTranslatef(x, y, z);
    glRotatef(angleX, 0.0, 1.0, 0.0);
    glRotatef(angleY, 0.0, 0.0, 1.0);
    //    glRotatef(angleZ, 0.0, 0.0, 1.0);

    glEnableClientState(GL_TEXTURE_COORD_ARRAY);
    glEnableClientState(GL_VERTEX_ARRAY);

    glTexCoordPointer(2, GL_FLOAT, 0, cubeTexcoords);
    glVertexPointer(3, GL_FLOAT, 0, cubeVertices);

    for (int i = 0; i < 24; i += 4) {


        //TEXTURE STUFF
        GLfloat face[4];
        memcpy(face, cubeIndices + i, 4);

        glEnable(GL_TEXTURE_2D);

        if (0 == i || 4 == i) {
            glBindTexture(GL_TEXTURE_2D, textureId2);
        } else {
            glBindTexture(GL_TEXTURE_2D, textureId1);
        }

        glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glColor3f(1.0f, 1.0f, 1.0f);

        glDrawElements(GL_QUADS, 4, GL_UNSIGNED_BYTE, face);

    }
    glDisableClientState(GL_VERTEX_ARRAY);
    //glDisableClientState(GL_COLOR_ARRAY);
    glDisable(GL_TEXTURE_2D);

    glPopMatrix();
}
//
//void OwnCube::setPosition(GLshort x, GLshort y, GLshort z) {
//
//    this->x = x;
//    this->y = y;
//    this->z = z;
//
//}

void OwnCube::setTextId(GLuint txt, GLuint txt2) {
    textureId1 = txt;
    textureId2 = txt2;
}



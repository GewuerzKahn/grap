/* 
 * File:   OwnTetrahedron.h
 * Author: student
 *
 * Created on September 17, 2014, 2:29 PM
 */

#ifndef OWNTETRAHEDRON_H
#define	OWNTETRAHEDRON_H

#include <GLUT/glut.h>

#include "OwnObject.h"

class OwnTetrahedron : public OwnObject{
public:
    OwnTetrahedron();
    OwnTetrahedron(const OwnTetrahedron& orig);
    virtual ~OwnTetrahedron();
    void draw();

private:

    GLfloat vertices[12] = {
        0.5f, 0.5f, 0.5f,
        -0.5f, 0.5f, -0.5f,
        0.5f, -0.5f, -0.5f,
        -0.5f, -0.5f, 0.5f,
    };


    GLshort indices[12] = {
        1, 2, 3,
        0, 3, 2,
        0, 1, 3,
        0, 2, 1
    };

};

#endif	/* OWNTETRAHEDRON_H */


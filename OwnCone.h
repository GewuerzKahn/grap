/* 
 * File:   OwnCone.h
 * Author: student
 *
 * Created on September 17, 2014, 9:27 PM
 */

#ifndef OWNCONE_H
#define	OWNCONE_H

#include <GLUT/glut.h>

#include "OwnObject.h"

class OwnCone : public OwnObject{
public:
    OwnCone();
    OwnCone(const OwnCone& orig);
    virtual ~OwnCone();

    void draw();
    
private:

};

#endif	/* OWNCONE_H */


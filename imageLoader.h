/* 
 * File:   imageLoader.h
 * Author: student
 *
 * Created on October 1, 2014, 1:32 PM
 */

#ifndef IMAGELOADER_H
#define	IMAGELOADER_H


#include <GLUT/glut.h>
#include <stdlib.h>
#include <string>
#include <map>


using namespace std;

class OwnImage {
public:
    OwnImage(char* ps, int w, int h);
    ~OwnImage();
    /* An array of the form (R1, G1, B1, R2, G2, B2, ...) indicating the
     * color of each pixel in image.  Color components range from 0 to 255.
     * The array starts the bottom-left pixel, then moves right to the end
     * of the row, then moves up to the next column, and so on.  This is the
     * format in which OpenGL likes images.
     */
    char* pixels;
    int width;
    int height;
};

class ImageLoader {
private:
    map< string, GLuint > textures;


    OwnImage* loadBMP(const char*);
    GLuint loadTexture(OwnImage*);
    void loadImages();
public:
    ImageLoader();
    GLuint getTexture(string);
};

OwnImage* loadBMP(const char* filename);
#endif	/* IMAGELOADER_H */


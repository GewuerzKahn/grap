/* 
 * File:   OwnCube.h
 * Author: student
 *
 * Created on September 17, 2014, 1:13 PM
 */

#ifndef OWNCUBE_H
#define	OWNCUBE_H

#include <GLUT/glut.h>
#include <stdio.h>
#include <string.h>
#include "OwnObject.h"
#include "loadBMP.h"

class OwnCube : public OwnObject {
public:
    OwnCube();
    OwnCube(const OwnCube& orig);
    virtual ~OwnCube();

    void draw();
    void drawTexture();
    void setTextId(GLuint txt, GLuint txt2);

private:
    GLuint textureId1;
    GLuint textureId2;
    
    void setUpTexture();
    //
    //    GLfloat vertices[24] = {
    //        -0.5f, -0.5f, 0.5f,
    //        -0.5f, 0.5f, 0.5f,
    //        0.5f, 0.5f, 0.5f,
    //        0.5f, -0.5f, 0.5f,
    //        -0.5f, -0.5f, -0.5f,
    //        -0.5f, 0.5f, -0.5f,
    //        0.5f, 0.5f, -0.5f,
    //        0.5f, -0.5f, -0.5f
    //    };

    GLfloat vertices[24] = {
        -0.5, -0.5, -0.5,
        0.5, -0.5, -0.5,
        0.5, 0.5, -0.5,
        -0.5, 0.5, -0.5,
        -0.5, -0.5, 0.5,
        0.5, -0.5, 0.5,
        0.5, 0.5, 0.5,
        -0.5, 0.5, 0.5
    };

    GLshort indices[24] = {
        0, 3, 2, 1,
        2, 3, 7, 6,
        0, 4, 7, 3,
        1, 2, 6, 5,
        4, 5, 6, 7,
        0, 1, 5, 4
    };

    GLfloat normals[24] = {
        -1.0, -1.0, -1.0,
        1.0, -1.0, -1.0,
        1.0, 1.0, -1.0,
        -1.0, 1.0, -1.0,
        -1.0, -1.0, 1.0,
        1.0, -1.0, 1.0,
        1.0, 1.0, 1.0,
        -1.0, 1.0, 1.0
    };

    GLfloat cubeVertices[16 * 3] = {
        -0.5f, 0.0f, 0.5f,
        0.5f, 0.0f, 0.5f,
        0.5f, 1.0f, 0.5f,
        -0.5f, 1.0f, 0.5f,
        -0.5f, 1.0f, -0.5f,
        0.5f, 1.0f, -0.5f,
        0.5f, 0.0f, -0.5f,
        -0.5f, 0.0f, -0.5f,
        0.5f, 0.0f, 0.5f,
        0.5f, 0.0f, -0.5f,
        0.5f, 1.0f, -0.5f,
        0.5f, 1.0f, 0.5f,
        -0.5f, 0.0f, -0.5f,
        -0.5f, 0.0f, 0.5f,
        -0.5f, 1.0f, 0.5f, -
        0.5f, 1.0f, -0.5f
    };

    GLfloat cubeTexcoords[8 * 4] = {
        0.0, 0.0, 1.0, 0.0, 1.0, 1.0, 0.0, 1.0,
        0.0, 0.0, 1.0, 0.0, 1.0, 1.0, 0.0, 1.0,
        0.0, 0.0, 1.0, 0.0, 1.0, 1.0, 0.0, 1.0,
        0.0, 0.0, 1.0, 0.0, 1.0, 1.0, 0.0, 1.0
    };

    GLubyte cubeIndices[24] = {0, 1, 2, 3, 4, 5, 6, 7, 3, 2, 5, 4, 7, 6, 1, 0,
        8, 9, 10, 11, 12, 13, 14, 15};

};

#endif	/* OWNCUBE_H */


/* 
 * File:   OwnCone.cpp
 * Author: student
 * 
 * Created on September 17, 2014, 9:27 PM
 */

#include "OwnCone.h"

OwnCone::OwnCone() {
}

OwnCone::OwnCone(const OwnCone& orig) {
}

OwnCone::~OwnCone() {
}

void OwnCone::draw() {
    glPushMatrix();
    
    materials(&material);

    glTranslated(x, y, z);
    glRotated(180, -1.0, 0.0, 0.0);
    glutSolidCone(1, 2, 50, 50);
    
    materials(&noMaterials);
            
    glPopMatrix();
    
    
}




/* 
 * File:   OwnCheckerboard.cpp
 * Author: student
 * 
 * Created on September 23, 2014, 5:58 PM
 */

#include "OwnCheckerboard.h"

OwnCheckerboard::OwnCheckerboard() {
    size = 64;
}

OwnCheckerboard::OwnCheckerboard(const OwnCheckerboard& orig) {
}

OwnCheckerboard::~OwnCheckerboard() {
}

void OwnCheckerboard::draw() {

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_COLOR, GL_DST_COLOR);

    GLubyte checkXor[512];
    GLubyte checkXorResult[512];

    //        GLubyte wb[2] = {0x00, 0xff};
    //        
    //            int i, j;
    //            int size = 32;
    //            int sizeSqrt = sqrt(size);
    //        
    //        
    //            GLubyte check[size * sizeSqrt];
    //        
    //            for (i = 0; i < size; i++) {
    //                for (j = 0; j < size / 8; j++) {
    //                    check[i * (size / 8) + j] = wb[(i / 8 + j) % 2];
    //                }
    //            }
    //        
    //            for (i = 0; i < 512; ++i) {
    //                checkXorResult[i] = check[i] ^ check[i];
    //            }
    //    
    //            glColor3f(1.0, 0.0, 0.0);
    //            glRasterPos3f(x, y, 0);
    //            glBitmap(size, size, 0.0, 0.0, 0.0, 0.0, check);
    //        
    //        
    //            glColor3f(0.0, 1.0, 0.0);
    //            glRasterPos3f(x - 0.10f, y + 0.12f, 0.02f);
    //            glBitmap(size, size, x + 0.04f, y + 0.04f, 0.0, 0.0, check);
    //

    //
#define	checkImageWidth size
#define	checkImageHeight size

    GLubyte checkImage[checkImageHeight][checkImageWidth][3];
    GLubyte checkImage2[checkImageHeight][checkImageWidth][3];

    int i, j, c;
    for (i = 0; i < checkImageHeight; i++) {
        for (j = 0; j < checkImageWidth; j++) {
            c = ((((i & 0x8) == 0)^((j & 0x8)) == 0)) * 255;
            if (!c) {
                checkImage[i][j][0] = (GLubyte) c;
                checkImage[i][j][1] = (GLubyte) c;
                checkImage[i][j][2] = (GLubyte) c;

                checkImage2[i][j][0] = (GLubyte) c;
                checkImage2[i][j][1] = (GLubyte) c;
                checkImage2[i][j][2] = (GLubyte) c;

            } else {
                checkImage[i][j][0] = 255;
                checkImage[i][j][1] = 0;
                checkImage[i][j][2] = 0;

                checkImage2[i][j][0] = 0;
                checkImage2[i][j][1] = 255;
                checkImage2[i][j][2] = 0;
            }
        }
    }

    glColor3f(1.0, 0.0, 0.0);
    glRasterPos2i(0, 0);
    glDrawPixels(checkImageWidth, checkImageHeight, GL_RGB,
            GL_UNSIGNED_BYTE, checkImage);

    glColor3f(0.0, 1.0, 0.0);
    glRasterPos3f(x - 0.10f, y + 0.12f, 0.02f);
    glDrawPixels(checkImageWidth, checkImageHeight, GL_RGB,
            GL_UNSIGNED_BYTE, checkImage2);

    glDisable(GL_BLEND);

    glRasterPos2i(15, 5);
    glPixelZoom(2.0, 2.0);
    glCopyPixels(15, 15, checkImageWidth + 15, checkImageHeight + 15, GL_COLOR);
    glPixelZoom(1.0, 1.0);

}

void OwnCheckerboard::setSize(int size) {
    this->size = size;
}

/* 
 * File:   OwnSquare.cpp
 * Author: student
 * 
 * Created on September 11, 2014, 9:19 PM
 */

#include "OwnSquare.h"

OwnSquare::OwnSquare() {

    squareState = 1;
    IsDiagonalsVisible = true;
    lineWidth = 1.0f;
}

OwnSquare::OwnSquare(const OwnSquare& orig) {
}

OwnSquare::~OwnSquare() {
}

void OwnSquare::draw() {

    glPushMatrix();

   // glTranslatef(0, 0, -2);

    if (squareState == 3) {
        glLineStipple(1, 0x00FF);
        glEnable(GL_LINE_STIPPLE);
    }

    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_COLOR_ARRAY);
    
    glColorPointer(3, GL_FLOAT, 0, colors);

    if (squareState == 2) {
        glVertexPointer(3, GL_FLOAT, 0, vertices);
        glDrawArrays(GL_QUADS, 0, 4);
    } else if (squareState == 1 || squareState == 3) {
        glVertexPointer(3, GL_FLOAT, 0, vertices);
        glDrawArrays(GL_LINE_LOOP, 0, 4);
    }

    glDisableClientState(GL_VERTEX_ARRAY);
    glDisableClientState(GL_COLOR_ARRAY);

    glDisable(GL_LINE_STIPPLE);
    glPopMatrix();


    if (IsDiagonalsVisible) {
        glPushMatrix();
        glLineStipple(1, 0xAAAA);
        glEnable(GL_LINE_STIPPLE);
      //  glTranslatef(0, 0, -2);

        glEnableClientState(GL_VERTEX_ARRAY);

        glVertexPointer(3, GL_FLOAT, 0, verticesDiagonal);
        glDrawArrays(GL_LINES, 0, 4);

        glDisableClientState(GL_VERTEX_ARRAY);
        glDisable(GL_LINE_STIPPLE);



        glPopMatrix();

        glLineWidth(lineWidth);

    }
}

void OwnSquare::changeSquareState(int state) {
    squareState = state;
}

void OwnSquare::setDiagonalsVisbility(bool isVisible) {
    IsDiagonalsVisible = isVisible;
}

void OwnSquare::setLineWidth(float width) {
    lineWidth = width;
}

bool OwnSquare::isDiagonalsVisible() {
    return IsDiagonalsVisible;
}




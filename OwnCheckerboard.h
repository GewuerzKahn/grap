/* 
 * File:   OwnCheckerboard.h
 * Author: student
 *
 * Created on September 23, 2014, 5:58 PM
 */

#ifndef OWNCHECKERBOARD_H
#define	OWNCHECKERBOARD_H

#include <GLUT/glut.h>

#include "OwnObject.h"
#include <math.h>

class OwnCheckerboard : public OwnObject {
public:
    OwnCheckerboard();
    OwnCheckerboard(const OwnCheckerboard& orig);
    virtual ~OwnCheckerboard();
    void draw();
    
    void setSize(int size);
    
private:
    int size;

};

#endif	/* OWNCHECKERBOARD_H */


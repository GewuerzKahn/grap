/* 
 * File:   OwnObject.h
 * Author: student
 *
 * Created on September 17, 2014, 9:31 PM
 */

#include <GLUT/glut.h>

#include "enums.h"

#ifndef OWNOBJECT_H
#define	OWNOBJECT_H

class OwnObject {
public:
    OwnObject();
    OwnObject(const OwnObject& orig);
    virtual ~OwnObject();

    void draw();
    void setPosition(GLfloat x, GLfloat y, GLfloat z);
    void getPosition(GLfloat& x, GLfloat& y, GLfloat& z);
    void setScale(GLfloat x, GLfloat y, GLfloat z);
    void setMaterial(EnumMaterial m);
    void setAngle(GLfloat x, GLfloat y, GLfloat z);
    void getAngle(GLfloat& x, GLfloat& y, GLfloat& z);


protected:
    GLfloat angle;
    GLfloat x, y, z;
    GLfloat scaleX, scaleY, scaleZ;
    GLfloat angleX, angleY, angleZ;

    typedef struct materialStruct {
        GLfloat ambient[4];
        GLfloat diffuse[4];
        GLfloat specular[4];
        GLfloat shininess;
    } materialStruct;

    materialStruct material;

    materialStruct brassMaterials = {
        {0.33, 0.22, 0.03, 1.0},
        {0.78, 0.57, 0.11, 1.0},
        {0.99, 0.91, 0.81, 1.0},
        27.8
    };

    materialStruct redPlasticMaterials = {
        {0.3, 0.0, 0.0, 1.0},
        {0.6, 0.0, 0.0, 1.0},
        {0.8, 0.6, 0.6, 1.0},
        32.0
    };

    materialStruct noMaterials = {
        {0.2, 0.2, 0.2, 1.0},
        {1.0, 1.0, 1.0, 1.0},
        {0.0, 0.0, 0.0, 0.0},
        0
    };

    void materials(materialStruct *materials) {
        glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, materials->ambient);
        glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, materials->diffuse);
        glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, materials->specular);
        glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, materials->shininess);
    }



private:




};

#endif	/* OWNOBJECT_H */


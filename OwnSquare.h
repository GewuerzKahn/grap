/* 
 * File:   OwnSquare.h
 * Author: student
 *
 * Created on September 11, 2014, 9:19 PM
 */

#ifndef OWNSQUARE_H
#define	OWNSQUARE_H

#include <GLUT/glut.h>

class OwnSquare {
public:
    OwnSquare();
    OwnSquare(const OwnSquare& orig);
    void draw();

    void changeSquareState(int state);
    void setDiagonalsVisbility(bool isVisible);
    void setLineWidth(float width);
    bool isDiagonalsVisible();

    virtual ~OwnSquare();
private:

    int squareState;
    bool IsDiagonalsVisible;
    float lineWidth;

    GLfloat vertices[12] = {
        -1.0f, 1.0f, 0.0f,
        -1.0f, -1.0f, 0.0f,
        1.0f, -1.0f, 0.0f,
        1.0f, 1.0f, 0.0f
    };

    GLfloat verticesDiagonal[12] = {
        -1.0f, -1.0f, 0.0f,
        1.0f, 1.0f, 0.0f,
        1.0f, -1.0f, 0.0f,
        -1.0f, 1.0f, 0.0f
    };

    GLfloat verticesStipple[24] = {
        -1.0f, -1.0f, 0.0f,
        -1.0f, 1.0f, 0.0f,

        -1.0f, 1.0f, 0.0f,
        1.0f, 1.0f, 0.0f,

        1.0f, 1.0f, 0.0f,
        1.0f, -1.0f, 0.0f,


        1.0f, -1.0f, 0.0f,
        -1.0f, -1.0f, 0.0f

    };

    GLfloat colors[24] = {
        1.0, 0.0, 0.0,
        0.0, 1.0, 0.0,
        1.0, 1.0, 0.0,
        1.0, 0.0, 0.0,
        1.0, 0.0, 0.0,
        0.0, 1.0, 0.0,
        1.0, 1.0, 0.0,
        1.0, 0.0, 0.0,
    };
};

#endif	/* OWNSQUARE_H */


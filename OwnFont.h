/* 
 * File:   OwnFont.h
 * Author: student
 *
 * Created on October 5, 2014, 1:44 PM
 */

#ifndef OWNFONT_H
#define	OWNFONT_H

#include <GLUT/glut.h>

#include <stdio.h>

//#include "letter_b.h"
#include "OwnObject.h"


#define OUTER_CPTS  13 * 3           /* Fixed maximum number of control points for outer contour */
#define DOT_CPTS  5 * 3           /* Fixed maximum number of control points for inner contour */

class OwnFont : public OwnObject {
public:
    OwnFont();
    OwnFont(const OwnFont& orig);
    virtual ~OwnFont();

    void drawCurves();
    void draw();

private:
    int ncpts = OUTER_CPTS + DOT_CPTS;
    int fontWidth = 500, fontHeight = 500;


    GLfloat cptsB[OUTER_CPTS + DOT_CPTS][3] = {
        //outer control points
        {1.0, 27.3, 0.0},
        {0.5, 28.0, 0.0},
        {1.0, 27.3, 0.0}, //left top serif

        {0.5, 28.0, 0.0},
        {6.2, 30.5, 0.0},
        {0.5, 28.0, 0.0}, //upper stroke left

        {6.2, 30.5, 0.0},
        {7.1, 30.5, 0.0},
        {6.2, 30.5, 0.0}, //upper stroke horizontal

        {7.1, 30.5, 0.0},
        {7.1, 17.1, 0.0},
        {7.1, 30.5, 0.0}, //upper right stroke down

        {7.1, 17.1, 0.0},
        {9.7, 20.7, 0.0},
        {12.5, 20.7, 0.0}, //upper left belly outside

        {13.2, 20.7, 0.0},
        {16.0, 20.7, 0.0},
        {20.2, 20.7, 0.0}, //upper right belly outside
        {20.2, 11.5, 0.0},
        {20.2, 0.6, 0.0},
        {13.0, 0.6, 0.0}, //lower right belly outside
        {10.3, 0.6, 0.0},
        {8.0, 0.6, 0.0},
        {6.2, 0.6, 0.0}, //lower left belly outside
        {3.7, 3.2, 0.0},
        {3.7, 24.0, 0.0},
        {3.7, 3.2, 0.0}, //left stroke up
        {3.7, 24.0, 0.0},
        {3.7, 28.8, 0.0},
        {3.7, 28.8, 0.0}, //closing left top serif
        {1.0, 27.3, 0.0},

        //inner control points
        {7.1, 15.4, 0.0},
        {9.4, 17.8, 0.0},
        {11.1, 17.8, 0.0}, //upper left belly inside
        {11.8, 17.8, 0.0},
        {13.7, 17.8, 0.0},
        {16.7, 17.8, 0.0}, //upper right belly inside
        {16.7, 10.5, 0.0},
        {16.7, 2.2, 0.0},
        {13.7, 2.2, 0.0}, //lower right belly inside
        {11.0, 2.2, 0.0},
        {10.0, 2.2, 0.0},
        {9.0, 2.2, 0.0}, //lower left belly inside
        {7.1, 4.2, 0.0},
        {7.1, 15.4, 0.0},
        {7.1, 4.2, 0.0}, //closing left stem inside
        {7.1, 15.4, 0.0}
    };

    GLfloat cpts[OUTER_CPTS + DOT_CPTS][3] = {
        //outer control points

        //left top serif
        {1.0, 16.6, 0.0},
        {0.8, 17.2, 0.0},
        {1.0, 16.6, 0.0},

        //upper stroke left
        {0.8, 17.2, 0.0},
        {6.0, 19.5, 0.0},
        {0.8, 17.2, 0.0},

        //upper stroke horizontal
        {6.0, 19.5, 0.0},
        {7.0, 19.5, 0.0},
        {6.0, 19.5, 0.0},

        //upper right stroke down
        {7.0, 19.5, 0.0},
        {7.0, 5.0, 0.0},
        {7.0, 19.5, 0.0},

        //down right belly outside
        {7.0, 5.0, 0.0},
        {7.0, 1.0, 0.0},
        {10.0, 1.6, 0.0},

        //down right small line
        {10.0, 1.6, 0.0},
        {10.0, 1.0, 0.0},
        {10.0, 1.6, 0.0},

        //bottom line
        {10.0, 1.0, 0.0},
        {1.0, 1.0, 0.0},
        {10.0, 1.0, 0.0},

        //down left small line
        {1.0, 1.0, 0.0},
        {1.0, 1.6, 0.0},
        {1.0, 1.0, 0.0},

        //down left belly outside
        {1.0, 1.6, 0.0},
        {4.0, 1.0, 0.0},
        {4.0, 5.0, 0.0},

        //left line
        {4.0, 5.0, 0.0},
        {4.0, 13.0, 0.0},
        {4.0, 5.0, 0.0},

        //upper left belly outside
        {4.0, 13.0, 0.0},
        {4.0, 18.0, 0.0},
        {2.0, 17.0, 0.0},

        //small upper left
        {2.0, 17.0, 0.0},
        {1.0, 16.6, 0.0},
        {2.0, 17.0, 0.0},

        //small upper left
        {1.0, 16.6, 0.0},
        {0.8, 17.2, 0.0},
        {1.0, 16.6, 0.0},



        //dot control points

        {3.2, 28.0, 0.0},
        {3.2, 30.0, 0.0},
        {5.2, 30.0, 0.0},

        {5.2, 30.0, 0.0},
        {7.2, 30.0, 0.0},
        {7.2, 28.0, 0.0},

        {7.2, 28.0, 0.0},
        {7.2, 26.0, 0.0},
        {5.2, 26.0, 0.0},
       
        {5.2, 26.0, 0.0},
        {3.2, 26.0, 0.0},
        {3.2, 28.0, 0.0},
        
        {3.2, 28.0, 0.0},
    };

};

#endif	/* OWNFONT_H */


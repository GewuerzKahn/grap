/* 
 * File:   OwnPolygon.cpp
 * Author: student
 * 
 * Created on September 11, 2014, 9:35 PM
 */

#include "OwnPolygon.h"

OwnPolygon::OwnPolygon() {

   
}

OwnPolygon::OwnPolygon(const OwnPolygon& orig) {
}

OwnPolygon::~OwnPolygon() {
}

void OwnPolygon::draw() {
    glPushMatrix();

   // glTranslatef(0, 0, -2);

    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_COLOR_ARRAY);

    glColorPointer(3, GL_FLOAT, 0, colors);
    glVertexPointer(3, GL_FLOAT, 0, vertices);
    glDrawArrays(GL_POLYGON, 0, 6);

    glDisableClientState(GL_VERTEX_ARRAY);
    glDisableClientState(GL_COLOR_ARRAY);
    glPopMatrix();
}


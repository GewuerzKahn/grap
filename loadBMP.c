/*
 * File:   loadBMP.c
 * Author: dos
 *
 * Created on July 6, 2009, 7:29 PM
 * Based on function found in Ulrich Jacobs final assignment, no source given
 */

#include "loadBMP.h"
#include <stdio.h>
#include <stdlib.h>

#define DEBUG 0

int bmpLoad(char* filename, Image *image) {
  FILE *file;
  unsigned long size;
  unsigned long i;
  unsigned short int planes;
  unsigned short int bpp;
  char temp;

  //Test if file exits
  if ((file = fopen(filename, "rb")) == NULL) {
    printf("File doesn't exist : %s\n", filename);
    return 0;
  }

  // TODO: test if has valid bmp format

  // skip bmp header until hight and width
  fseek(file, 18, SEEK_CUR);

  // read width
  if ((i = fread(&image->sizeX, 4, 1, file)) != 1) {
    printf("Width couldn't be read.\n");
    return 0;
  }
  if (DEBUG) printf("Width = %lu\n", image->sizeX);

  // read height
  if ((i = fread(&image->sizeY, 4, 1, file)) != 1) {
    printf("Height couldn't be read.\n");
    return 0;
  }
  if (DEBUG) printf("Height = %lu\n", image->sizeY);

  // calculate size (3 Bytes per Pixel).
  size = image->sizeX * image->sizeY * 3;

  // read number of planes
  if ((fread(&planes, 2, 1, file)) != 1) {
    printf("Number of planes couldn't be read");
    return 0;
  }

  if (planes != 1) {
    printf("Number of planes is not = 1, but %u\n", planes);
    return 0;
  }

  // read bits per pixel
  if ((i = fread(&bpp, 2, 1, file)) != 1) {
    printf("Couldn't read bits per pixel.\n");
    return 0;
  }
  if (bpp != 24) {
    printf("Bpp is not 24, but  %u\n", bpp);
    return 0;
  }

  // jump to the end of the header
  fseek(file, 24, SEEK_CUR);

  // read the image
  image->data = (GLubyte *) malloc(size);
  if (image->data == NULL) {
    printf("Couldn't allocate memory.\n");
    return 0;
  }

  if ((i = fread(image->data, size, 1, file)) != 1) {
    printf("Image read error.\n");
    return 0;
  }

  // swap colors from BGR to RGB
  for (i = 0; i < size; i += 3) {
    temp = image->data[i];
    image->data[i] = image->data[i + 2];
    image->data[i + 2] = temp;
  }

  return 1;
}
